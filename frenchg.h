! ==============================================================================
!   GRAMMAR:  Grammar table entries for the standard verbs library.
!
!   Supplied for use with Inform 6 -- Release 6.12.6 -- Serial number 220219
!
!   Copyright Graham Nelson 1993-2004 and David Griffith 2012-2022
!   This code is licensed under either the traditional Inform license as
!   described by the DM4 or the Artistic License version 2.0.  See the
!   file COPYING in the distribution archive or at
!   https://gitlab.com/DavidGriffith/inform6lib/
!
!   In your game file, Include three library files in this order:
!       Include "Parser";
!       Include "VerbLib";
!       Include "Grammar";
! ==============================================================================
! Lionel Ange, 2008-2022 pour la traduction et l'adaptation en français.
! Version 1.1.0
! Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0) 
! https://creativecommons.org/licenses/by-sa/4.0/deed.fr
! ==============================================================================
! Source encoding UTF-8 No Mark

System_file;

#Ifdef      LIBRARY_STAGE;
#Iffalse LIBRARY_STAGE >= AFTER_GRAMMAR;  ! if not already included
#Iftrue     LIBRARY_STAGE == AFTER_VERBLIB;  ! if okay to include it

! ------------------------------------------------------------------------------
!  The "meta-verbs", commands to the game rather than in the game, come first:
! ------------------------------------------------------------------------------
! Les meta-verbs ont été réorganisés et diffèrent de la version anglaise.

Verb meta 'description'
   *                                           -> LModeNormal
   * 'longue'                                  -> LMode1
   * 'courte'                                  -> LMode1
   * 'brève'                                   -> LMode3
   * 'normale'                                 -> LModeNormal;

Verb meta 'pronoms'
   *                                           -> Pronouns;

Verb meta 'q//' 'mourir'
   *                                           -> Quit;

Verb 'commandes'
   *                                           -> CommandsOn
   * 'activer'/'on'                            -> CommandsOn
   * 'désactiver'/'off'                        -> CommandsOff
   * 'rejouer'                                 -> CommandsRead;

Verb meta 'recommencer'
   *                                           -> Restart;

Verb meta 'charger'
   *                                           -> Restore;

Verb meta 'sauvegarder'
   *                                           -> Save;

Verb meta 'score'
   *                                           -> Score
   * 'total'                                   -> FullScore ! complet
   * 'activer'/'on'                            -> NotifyOn
   * 'désactiver'/'off'                        -> NotifyOff;

Verb meta 'script' 'transcription'
   *                                           -> ScriptOn
   * 'activer'/'on'                            -> ScriptOn
   * 'désactiver'/'off'                        -> ScriptOff;

Verb meta 'vérifier'
   *                                           -> Verify;

Verb meta 'version'
   *                                           -> Version;

#Ifndef NO_PLACES;
Verb meta 'objets'
   *                                           -> Objects
   * 'colonne'                                 -> ObjectsTall
   * 'ligne'                                   -> ObjectsWide
   * 'en' 'colonne'                            -> ObjectsTall
   * 'en' 'ligne'                              -> ObjectsWide;
Verb meta 'lieux'
   *                                           -> Places
   * 'colonne'                                 -> PlacesTall
   * 'ligne'                                   -> PlacesWide
   * 'en' 'colonne'                            -> PlacesTall
   * 'en' 'ligne'                              -> PlacesWide;
#Endif; ! NO_PLACES

! ------------------------------------------------------------------------------
!  Debugging grammar
! ------------------------------------------------------------------------------

#Ifdef DEBUG;
Verb meta 'actions'
   *                                           -> ActionsOn
   * 'on'                                      -> ActionsOn
   * 'off'                                     -> ActionsOff;

Verb meta 'changes'
   *                                           -> ChangesOn
   * 'on'                                      -> ChangesOn
   * 'off'                                     -> ChangesOff;

Verb meta 'gonear'
   * anynumber                                 -> GoNear
   * noun                                      -> GoNear;

Verb meta 'goto'
   * anynumber                                 -> Goto;

Verb meta 'random'
   *                                           -> Predictable;

Verb meta 'routines' 'messages'
   *                                           -> RoutinesOn
   * 'on'                                      -> RoutinesOn
   * 'verbose'                                 -> RoutinesVerbose
   * 'off'                                     -> RoutinesOff;

Verb meta 'scope'
   *                                           -> Scope
   * anynumber                                 -> Scope
   * noun                                      -> Scope;

Verb meta 'showdict' 'dict'
   *                                           -> ShowDict
   * topic                                     -> ShowDict;

Verb meta 'showobj'
   *                                           -> ShowObj
   * anynumber                                 -> ShowObj
   * multi                                     -> ShowObj;

Verb meta 'showverb'
   * special                                   -> ShowVerb;

Verb meta 'timers' 'daemons'
   *                                           -> TimersOn
   * 'on'                                      -> TimersOn
   * 'off'                                     -> TimersOff;

Verb meta 'trace'
   *                                           -> TraceOn
   * number                                    -> TraceLevel
   * 'on'                                      -> TraceOn
   * 'off'                                     -> TraceOff;

Verb meta 'abstract'
   * anynumber 'to' anynumber                  -> XAbstract
   * noun 'to' noun                            -> XAbstract;

Verb meta 'purloin'
   * anynumber                                 -> XPurloin
   * multi                                     -> XPurloin;

Verb meta 'tree'
   *                                           -> XTree
   * anynumber                                 -> XTree
   * noun                                      -> XTree;

#Ifdef TARGET_GLULX;
Verb meta 'glklist'
   *                                           -> Glklist;
#Endif; ! TARGET_

#Endif; ! DEBUG

! ------------------------------------------------------------------------------
!  And now the game verbs.
! ------------------------------------------------------------------------------
! Forme pronominale : 'soi'
! Les accents et les prépositions sont obligatoires. Il y a une tolérance pour 
! le verbe aller qui peut être utilisé sans préposition.
! Squeeze remplace Push pour appuyer_sur.
! ------------------------------------------------------------------------------
Dictionary 'quitter' DICT_META; ! utile ?

[ ADirection; if (noun in Compass) rtrue; rfalse; ];
[ sGoUpSub;   <<Go u_obj>>; ];
![ sGoDownSub; <<Go d_obj>>; ];
[ sGetOffSub   o;
   o = parent(player);
   if (o == location || (location == thedark && o == real_location))
      <<Go d_obj>>;
   <<GetOff (o)>>;
];
! [ sLever;  verb_word = 'soulever'; ];
[ VagueVerbSub; L__M(##VagueVerb); ];

Verb 'parler'
   * 'à'/'au'/'aux'/'avec' creature 
                          'de'/'d^'/'du'/'des' topic -> Tell
   * 'de'/'d^'/'du'/'des' topic
                     'à'/'au'/'aux'/'avec' creature  -> Tell reverse
;

Verb 'répondre'
   * 'à'/'au'/'aux' creature topic                   -> Answer reverse
   * topic 'à'/'au'/'aux' creature                   -> Answer
;

Verb 'demander'
   * noun 'à'/'au'/'aux' creature                    -> AskFor reverse
   * 'à'/'au'/'aux' creature noun                    -> AskFor
   * 'à'/'au'/'aux' creature 'de'/'d^' topic         -> AskTo
;

Verb 'dire'
   * 'à'/'au'/'aux' creature 'de'/'d^' topic         -> AskTo
   * 'à'/'au'/'aux' creature topic                   -> Answer reverse
   * topic 'à'/'au'/'aux' creature                   -> Answer
;

Verb 'interroger'
   * creature 'sur' topic                            -> Ask ! concernant
   * creature 'à' 'propos' 'de'/'d^'/'du'/'des' topic-> Ask ! au_sujet_de
;

Verb 'attaquer'
   * noun                                            -> Attack
;

Verb 'montrer'
   * held 'à'/'au'/'aux' creature                    -> Show
   * 'à'/'au'/'aux' creature held                    -> Show reverse
;

Verb 'réveiller'
   * 'soi'                                           -> Wake
   * creature                                        -> WakeOther
;

Verb 'embrasser'
   * creature                                        -> Kiss
;

Verb 'saluer'
   *                                                 -> WaveHands
   * noun                                            -> WaveHands
;

Verb 'regarder' 'l//' 'r//'
   *                                                 -> Look
   * noun                                            -> Examine
   * 'sur'/'dans'/'par'/'par-dessus' noun            -> Search ! derrière
   * 'à'/'au'/'en'/'vers' noun=ADirection            -> Examine ! compass_look
   * 'à'/'au' 'travers' noun                         -> Search
   * 'sous' noun                                     -> LookUnder
   * topic 'à'/'au'/'sur'/'dans' noun                -> Consult
;

Verb 'examiner' 'x//'
   * noun                                            -> Examine
;

Verb 'lire'
   * noun                                            -> Examine
   * noun 'sur'/'de'/'d^' topic                      -> Consult
   * topic 'au'/'sur'/'dans' noun                    -> Consult
;

Verb 'fouiller'
   * noun                                            -> Search
   * 'dans' noun                                     -> Search
;

Verb 'chercher' 'rechercher'
   * noun                                            -> VagueVerb
   * topic 'dans' noun                               -> Consult ! look_up
;

Verb 'aller' ! 'a//'
   *                                                 -> VagueGo
!  * noun=ADirection                                 -> Go !#
   * noun                                            -> Enter !#
   * 'à'/'au'/'en'/'vers' noun=ADirection            -> Go
   * 'à'/'au'/'aux'/'sur'/'dans'/'sous' noun         -> Enter
;

Verb 'entrer'
   *                                                 -> GoIn
   * 'dans'/'par'/'sur'/'sous' noun                  -> Enter
;

Verb 'sortir'
   *                                                 -> Exit ! de_là/d'ici
   * multiinside 'de'/'d^'/'du'/'des' noun           -> Remove
   * 'de'/'d^'/'du'/'des' noun                       -> Exit
   * 'par' noun                                      -> Enter
;

Verb 'passer'
   * 'sur'/'dans'/'par'/'sous' noun                  -> Enter
   * 'par-dessus' noun                               -> Enter
   * 'à' 'travers' noun                              -> Enter
;

Verb 'monter'
   *                                                 -> sGoUp
   * 'à'/'au'/'aux'/'sur'/'dans'/'par'/'en' noun     -> Enter
;

Verb 'descendre' 'd//'
   *                                                 -> sGetOff
   * 'de'/'d^'/'du'/'des' noun                       -> GetOff
   * 'à'/'au'/'aux'/'sur'/'dans'/'par' noun          -> Enter
;

Verb 'grimper'
   * 'sur'/'à'/'au'/'aux'/'dans'/'par' noun          -> Climb
;

Verb 'sauter'
   *                                                 -> Jump
!  * 'à' noun                                        -> Jump ! la corde
   * 'dans' noun                                     -> JumpIn
   * 'sur' noun                                      -> JumpOn
   * 'par-dessus' noun                               -> JumpOver
   * 'par' noun                                      -> Enter
!   * 'par'/'de'/'du'/'des' noun                      -> Exit !TODO
;

Verb 'asseoir' 'coucher' 'allonger' 
   * 'soi' 'sur'/'dans' noun                         -> Enter
;

Verb 'lever'
   * 'soi'                                           -> Exit
;

Verb 'nager'
   *                                                 -> Swim
;

Verb 'balancer' 'suspendre'
   * 'soi' 'à'/'au'/'aux'/'sur'/'dans' noun          -> Swing
;

Verb 'prendre'
   * multi                                           -> Take
   * multiinside 'à'/'au'/'aux' noun                 -> Remove
   * multiinside 'de'/'d^'/'du'/'des'/'dans'/'sur' 
                                              noun   -> Remove
;

Verb 'décoller' 'détacher'
   * noun                                            -> Take
;

Verb 'acheter'
   * noun                                            -> Buy
;

Verb 'donner'
   * held 'à'/'au'/'aux' creature                    -> Give
   * 'à'/'au'/'aux' creature held                    -> Give reverse
;

Verb 'lancer' 'jeter'
   * noun                                            -> ThrowAt
   * held 'à'/'au'/'aux' noun                        -> ThrowAt
   * held 'sur'/'dans'/'contre'/
                             'par'/'par-dessus' noun -> ThrowAt
   * 'à'/'au'/'aux' noun held                        -> ThrowAt reverse
;

Verb 'poser'
   * multiheld                                       -> Drop
   * multiexcept 'dans'/'sous'/'entre' noun          -> Insert
   * multiexcept 'sur' noun                          -> PutOn
;

Verb 'insérer' 'introduire'
   * multiexcept 'dans'/'sous'/'entre' noun          -> Insert
;

Verb 'mettre'
   * multiheld                                       -> Wear
   * 'en' 'marche'/'route' noun                      -> SwitchOn
   * noun 'en' 'marche'/'route'                      -> SwitchOn
   * multiexcept 'dans'/'sous'/'entre' noun          -> Insert
   * multiexcept 'sur' noun                          -> PutOn
;

Verb 'enlever' 'retirer'
   * worn                                            -> Disrobe
   * multi                                           -> Remove
   * multiinside 'de'/'d^'/'du'/'des' noun           -> Remove
;

Verb 'quitter'
   *                                                 -> Quit
   * multi                                           -> Disrobe
;

Verb 'remplir'
   * noun                                            -> Fill
   * noun 'de'/'d^'/'avec' noun                      -> Fill
;

Verb 'vider'
   * noun                                            -> Empty
   * noun 'sur'/'dans' noun                          -> EmptyT
;

Verb 'pousser'
   * noun                                            -> Push
   * noun 'à'/'au'/'en'/'vers' noun                  -> PushDir ! AllowPushDir()
   * noun 'sur'/'dans'/'contre' noun                 -> Transfer
;

Verb 'tirer'
   * noun                                            -> Pull
;

Verb 'manger'
   * held                                            -> Eat
;

Verb 'boire'
   * noun                                            -> Drink
;

Verb 'ouvrir'
   * noun                                            -> Open
   * noun 'avec' held                                -> Unlock
;

Verb 'fermer' 'refermer'
   * noun                                            -> Close
   * noun 'à'/'avec' held                            -> Lock
;

Verb 'verrouiller'
   * noun 'avec' held                                -> Lock
;

Verb 'déverrouiller'
   * noun 'avec' held                                -> Unlock
;

Verb 'forcer' 'fracturer'
   * noun 'avec' held                                -> Unlock
;

Verb 'écraser' 'presser'
   * noun                                            -> Squeeze
;

Verb 'appuyer'
   * 'sur' noun                                      -> Squeeze ! Remplace Push
;

Verb 'allumer' 'démarrer'
   * noun                                            -> SwitchOn
;

Verb 'éteindre' 'arrêter' 
   * noun                                            -> SwitchOff
;

Verb 'régler'
   * noun                                            -> Set
   * noun 'à'/'sur' special                          -> SetTo
;

Verb 'attacher'
   * noun                                            -> Tie
   * noun 'à'/'au'/'aux'/'avec'/'sur'/'dans' noun    -> Tie
;

Verb 'tourner'
   * noun                                            -> Turn
;

Verb 'agiter' 'secouer'
   * noun                                            -> Wave
;

Verb 'creuser'
   * noun                                            -> Dig
   * noun 'avec' held                                -> Dig
   * 'sur'/'dans' noun                               -> Dig
   * 'sur'/'dans' noun 'avec' held                   -> Dig
;

Verb 'couper' 'déchirer'
   * noun                                            -> Cut
;

Verb 'casser' 'détruire' 'briser'
   * noun                                            -> Attack
;

Verb 'brûler'
   * noun                                            -> Burn
   * noun 'avec' held                                -> Burn
;

Verb 'souffler'
!  * held                                            -> Blow !#
   * 'sur'/'dans' held                               -> Blow
;

Verb 'nettoyer'
   * noun                                            -> Rub
;

Verb 'utiliser' 'actionner' 'faire' 'déplacer' 
   * topic                                           -> VagueVerb
;

Verb 'toucher' 'caresser'
   * noun                                            -> Touch
;

Verb 'goûter' 
   * noun                                            -> Taste
;

Verb 'sentir'
   *                                                 -> Smell
   * noun                                            -> Smell
;

Verb 'écouter'
   *                                                 -> Listen
   * noun                                            -> Listen
;

Verb 'chanter'
   *                                                 -> Sing
;

Verb 'penser' 'réfléchir' 
   *                                                 -> Think
;

Verb 'prier'
   *                                                 -> Pray
;

Verb 'merde'
   *                                                 -> Strong
   * topic                                           -> Strong
;

Verb 'excuser'
   * 'soi'                                           -> Sorry
;

Verb 'zut'
   *                                                 -> Mild
   * topic                                           -> Mild
;

Verb 'dormir'
   *                                                 -> Sleep
;

Verb 'attendre' 'z//'
   *                                                 -> Wait
;

Verb 'oui'
   *                                                 -> Yes
;

Verb 'non' 
   *                                                 -> No
;

Verb 'inventaire' 'i//' 
   *                                                 -> Inv
   * 'colonne'                                       -> InvTall
   * 'ligne'                                         -> InvWide
   * 'en' 'colonne'                                  -> InvTall
   * 'en' 'ligne'                                    -> InvWide
;

! ------------------------------------------------------------------------------
!  This routine is no longer used here, but provided to help existing games
!  which use it as a general parsing routine:

![ ConTopic w;
!    consult_from = wn;
!    do w = NextWordStopped();
!    until (w == -1 || (w == 'to' && action_to_be == ##Answer));
!    wn--;
!    consult_words = wn - consult_from;
!    if (consult_words == 0) return -1;
!    if (action_to_be == ##Answer or ##Ask or ##Tell) {
!        w = wn; wn = consult_from; parsed_number = NextWord();
!        if (parsed_number == 'the' && consult_words > 1) 
!            parsed_number = NextWord();
!        wn = w;
!        return 1;
!    }
!    return 0;
!];

! ------------------------------------------------------------------------------
!  Final task: provide trivial routines if the user hasn't already:
! ------------------------------------------------------------------------------

Default Story          0;
Default Headline       0;
Default d_obj          NULL;
Default u_obj          NULL;

Stub AfterLife         0;
Stub AfterPrompt       0;
Stub Amusing           0;
Stub BeforeParsing     0;
Stub ChooseObjects     2;
Stub DarkToDark        0;
Stub DeathMessage      0;
Stub Epilogue          0;
Stub GamePostRoutine   0;
Stub GamePreRoutine    0;
Stub InScope           1;
Stub LookRoutine       0;
Stub NewRoom           0;
Stub ObjectDoesNotFit  2;
Stub ParseNumber       2;
Stub ParserError       1;
Stub PrintTaskName     1;
Stub PrintVerb         1;
Stub TimePasses        0;
Stub UnknownVerb       1;
Stub AfterSave         1;
Stub AfterRestore      1;

#Ifdef TARGET_GLULX;
Stub HandleGlkEvent    2;
Stub IdentifyGlkObject 4;
Stub InitGlkWindow     1;
#Endif; ! TARGET_GLULX

#Ifndef PrintRank;
[ PrintRank; "."; ];
#Endif;

#Ifndef ParseNoun;
[ ParseNoun obj; obj = obj; return -1; ];
#Endif;

#Ifdef INFIX;
Include "infix";
#Endif;

! ==============================================================================

Undef LIBRARY_STAGE; Constant LIBRARY_STAGE = AFTER_GRAMMAR;

#Ifnot;     ! LIBRARY_STAGE < AFTER_GRAMMAR but ~= AFTER_VERBLIB
Message "Erreur : 'verblib' doit être correctement inclus avant d'inclure 
'frenchg'. Sinon cela provoquera un grand nombre d'erreurs !";
#Endif;

#Ifnot;     ! LIBRARY_STAGE >= AFTER_GRAMMAR : already included
Message "Attention : le fichier 'frenchg' est inclus deux fois ; la deuxième 
inclusion est ignorée. (Oubliez ceci si c'est volontaire.)";
#Endif;

#Ifnot;     ! LIBRARY_STAGE is not defined
Message "Erreur : 'parser' doit être correctement inclus avant d'inclure 
'frenchg'. Sinon cela provoquera un grand nombre d'erreurs !";
#Endif;

! ==============================================================================

