Version 1.1.dev  
Lionel Ange, 2008-2022.  
Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0)  
https://creativecommons.org/licenses/by-sa/4.0/deed.fr  

Dépôt pour la bibliothèque 6.12.x en français :  
https://gitlab.com/auraes/inform6lib_fr

Traduction et adaptation de la bibliothèque anglaise 6.11 vers 6.12 d'Inform 6.
Seuls les fichiers english.h et grammar.h de la bibliothèque anglaise (6.12.x) ont été modifiés, et renommés respectivement french.h et frenchg.h auxquels est associé le fichier translation.h

Cette bibliothèque permet de créer, en langage Inform 6, des jeux d’aventure textuels en français.

L’adaptation ne prend pas en compte les changements apportés par David Griffith sur la version 6.12 anglaise, concernant le temps et le choix du sujet du protagoniste.  
https://gitlab.com/DavidGriffith/inform6lib  
et  
http://www.inform-fiction.org/index.html  
http://www.ifarchive.org/indexes/if-archiveXinfocomXcompilersXinform6.html

Pour débuter sous MS Windows, téléchargez l'archive libI6_Windows.zip

================================================================================

This is version 6.12.6 of the Inform Library,  
Copyright Graham Nelson 1993-2004, David Griffith 2012-2022  
Full release notes and instructions are available at  
http://www.inform-fiction.org  
http://www.ifarchive.org/indexes/if-archiveXinfocomXcompilersXinform6.html

The Git repository is at https://gitlab.com/DavidGriffith/inform6lib

================================================================================

La fiction interactive en langue française appartient à tous et pour tous. Profitez-en !

