! ==============================================================================
! Lionel Ange, 2008-2022
! Version 1.0.0
! Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0) 
! https://creativecommons.org/licenses/by-sa/4.0/
! ==============================================================================
! Source encoding UTF-8 No Mark

#Ifdef TARGET_ZCODE;
[ LenMot motn; return parse->(motn*4); ];    ! WordLength
[ PosMot motn; return parse->(motn*4+1); ];  ! WordAddress-buffer
[ DicMot motn; return parse-->(motn*2-1); ]; ! WordValue
[ NbrMot; return parse->1; ];                ! NumberWords
#Ifnot; ! TARGET_GLULX
[ LenMot motn; return parse-->(motn*3-1); ]; ! WordLength
[ PosMot motn; return parse-->(motn*3); ];   ! WordAddress-buffer
[ DicMot motn; return parse-->(motn*3-2); ]; ! WordValue
[ NbrMot; return parse-->0; ];               ! NumberWords
#Endif;

[ NbMotPhrase   motn i w;
   i = NbrMot();
   for (motn = 1 : motn <= i : motn++) {
      w = DicMot(motn);
      if (w == './/' or THEN1__WD or THEN2__WD or THEN3__WD)
         break;
   }
   return (motn - 1);
];

[ EffaceMot motn   at lgm;
   at = PosMot(motn);
   lgm = LenMot(motn) + at;
   while (at < lgm)
      buffer->at++ = ' ';
];

[ RemplaceMot motn chaine   at i lgm lgc; ! possible "   " pour éviter tokenise
    at = PosMot(motn);
    lgm = LenMot(motn) + at;
    lgc = PrintToBuffer(StorageForShortName, 160, chaine) + at;
    i = lgm;
    while (i++ < lgc)
       LTI_Insert(at, ' ');
    i = WORDSIZE;
    while (at < lgc)
       buffer->(at++) = StorageForShortName->(i++);
    while (at < lgm)
       buffer->(at++) = ' ';
];

[ InsertMot motn chaine   at i lgc; ! motn de 1 à NbrMotPhrase() + 1
   i = NbMotPhrase();
   if (motn > i) {
      motn = i;
      at = LenMot(motn);
   }
   at = at + PosMot(motn);
   LTI_Insert(at++, ' ');
   lgc = PrintToBuffer(StorageForShortName, 160, chaine) + at;
   i = WORDSIZE;
   while (at < lgc)
      LTI_Insert(at++, StorageForShortName->(i++));
   LTI_Insert(at, ' ');
];

! WARNING si mot >= 9
[ CoupeTiret motn   at lgm;
   at = PosMot(motn);
   lgm = LenMot(motn) + at;
   for ( : at < lgm : at++) {
      if ((buffer->at) == '-') {
         if (DictionaryLookup(buffer + at, lgm - at)) {
            LTI_Insert(at++,' ');
            rtrue;
         }
      }
   }
   rfalse;
];

[ decolleApostrophe motn   at lgm;
   at = PosMot(motn);
   lgm = LenMot(motn) + at;
   for ( : at < lgm : at++) {
      if ((buffer->at) == ''') {
         LTI_Insert(++at, ' ');
         rtrue; 
      }
   }
   rfalse;
];

#Ifdef TARGET_ZCODE;
[ FirstCapital w   i;
   if ( LanguageVerb(w, 0) ) return;
   @output_stream 3 StorageForShortName;
   print (address) w;
   @output_stream -3;
   print (char) UpperCase(StorageForShortName->(0+WORDSIZE));
   for (i = 1 : i < StorageForShortName-->0 : i++)
      print (char) StorageForShortName->(i+WORDSIZE);
];
#Ifnot; ! TARGET_GLULX
[ FirstCapital w;
   if ( LanguageVerb(w, 0) ) return;
   PrintToBuffer(StorageForShortName, 160, w);
   PrintFromBuffer(StorageForShortName, true, 1);
];
#Endif;

[ LanguageCommand from i k spacing_flag w flag;
   if (from == 0) {
      i = verb_word;
      if (LanguageVerb(i) == 0 && PrintVerb(i) == false
         && LibraryExtensions.RunWhile(ext_printverb, false, i) == 0)
         print (address) i;
      from++; spacing_flag = true;
   }
   for (k=from : k<pcount : k++) {
      i = pattern-->k;
      if (i == PATTERN_NULL) continue;
      if (spacing_flag && flag == false) print (char) ' ';
      if (i == 0) { print (string) THOSET__TX; jump TokenPrinted; }
      if (i == 1) { print (string) THAT__TX;   jump TokenPrinted; }
      if (i >= REPARSE_CODE) {
         w = No__Dword(i-REPARSE_CODE);
         if (w == 'à//' or 'de')
            flag = true;
         else
            print (address) w;
      }     
      else {
         if (i in compass && LanguageVerbLikesAdverb(verb_word))
            LanguageDirection (i.door_dir); ! the direction name as adverb
         else {
            if (flag) {
               if (w == 'à//') _aaux(i, 1); 
               else _dedudes(i, 1);
               print (name) i;
               flag = false;
            }  
            else {
               if (i in compass) print (a) i;
               else print (the) i;
            }
         }
      }
      .TokenPrinted;
      spacing_flag = true;
   }
];

[ PronomInterrogatif   i k w w1 f1 f2 f3 f4;
   for (k = 1 : k<pcount : k++) {
      i = pattern-->k;
      if (i == PATTERN_NULL) continue;
      if (i == 0 ) { f3 = true; continue; }
      if (i == 1)  { f4 = true; continue; }
      if (i >= REPARSE_CODE) {
         w = No__Dword(i-REPARSE_CODE);
         if (w == ME1__WD or ME2__WD or ME3__WD) { f1 = true; continue; }  
         if (f2 == 0) { 
            f2 = true;
            FirstCapital(w);
         }
         else print (address) w;
         print " ";
      }
      else w1 = i;
   }
   if (lm_n == 48) {
      if (f2) print "qui";
      else print "Qui";
   }
   else {
      if (f2) print "quoi";
      else print "Que";
   }
   print " voulez-vous ";
   if (actor ~= player) print "que ", (the) actor, " "; ! TODO conjuguer le verbe
   if (f1) print "vous ";
   if ( LanguageVerb(verb_word) == 0 ) print (address) verb_word;
   if (w1) print " ", (the) w1;
   if (f3) print " ", (string) THOSET__TX;
   if (f4) print " ", (string) THAT__TX;
];

[ Translation   n i p w w1 r;
   Tokenise__(buffer, parse); ! utile pour phrase incomplète
   
   n = NbMotPhrase();
   p = 1;
   if (n > 1 && DicMot(2) == ',//') { ! Toto, verbe
      if (n == 2) jump jlang;
      p = 3;
   }

   i = p;
   while (i <= n) {
      w = DicMot(i);
      if (w == 0) { ! WARNING si >= 9
         r = decolleApostrophe(i);
         if (r) {
            n++;
            Tokenise__(buffer,parse);
         }
      }
      i++;
   }
   
   i = p;
   while (i <= n) {
      r = LenMot(i);
      w = DicMot(i);
      if (w == 0 || r >= 9) { ! WARNING si >=9
         r = CoupeTiret(i);
         if (r) {
            n++;
            Tokenise__(buffer,parse);
            i--;
         }
      }
      i++;
   }

   w = DicMot(p);
   if (w == 'je' or 'j^' or 'tu') { ! 't^'
      EffaceMot(p);
      n--;
      Tokenise__(buffer,parse);
   }
   
   i = p;
   while (i <= n) {
      w = DicMot(i);
      switch (w) {
         '-moi', '-toi', '-soi': RemplaceMot(i, " soi");
         'moi': RemplaceMot(i, "soi");
      }
      i++;
   }
   Tokenise__(buffer, parse);
   
   if (n == p) jump jlang;
   
   w = DicMot(p);
   switch (w) {
      'me', 'te', 'se', 'm^', 't^', 's^':
         RemplaceMot(p, "soi");
         Tokenise__(buffer,parse);
   }

! Pronoms avant :
 
   w = DicMot(p);
   w1 = DicMot(p+1);
   
   r = 2;
   switch (w) {
      'soi':
         switch (w1) {
            'le':     w = "-le à soi"; 
            'la':     w = "-la à soi"; 
            'les':    w = "-les à soi";
            'l^':     w = "-l' à soi";
            'y//':    w = "soi dans -y"; !? vers
            'en':     w = "-en"; ! s'en aller
            default:  w = "soi"; r = 1; 
         }
      'l^':
         switch (w1) {
            'y//':    w = "-l' dans -y"; !? sur
            default:  w = "-l'"; r = 1;
         }
      'le':
         switch (w1) {
            'lui':    w = "-le à -lui"; 
            'leur':   w = "-le à eux";
            default:  w = "-le"; r = 1;
         }
      'la':
         switch (w1) {
            'lui':    w = "-la à -lui"; 
            'leur':   w = "-la à eux";
            default:  w = "-la"; r = 1;
         }
      'les':
         switch (w1) {
            'lui':    w = "-les à -lui"; 
            'leur':   w = "-les à eux";
            'y//':    w = "-les dans -y"; 
            default:  w = "-les"; r = 1;
         }
      'lui':
         switch (w1) {
            'en':     w = "-en à -lui";
            default:  w = "à -lui"; r = 1;
         }  
      'leur':
         switch (w1) {
            'en':     w = "-en à eux";
            default:  w = "à eux"; r = 1;
         }
      'y//':          w = "à -y"; r = 1; !? sur/dans
   
      'en':           w = "-en"; r = 1;
      
      default: r = 0;
   }
   
   if (r) {
      if (r == 1) {
         EffaceMot(p);
         InsertMot(p+2, w);
      }
      else {
         EffaceMot(p);
         EffaceMot(p+1);
         InsertMot(p+3, w);
      }
      jump jlang;
   }

! Pronoms après :

   w = DicMot(p+1);
   w1 = 0;
   if (n > p+1) w1 = DicMot(p+2);

   r = 2;   
   switch (w) {
      'soi': w = "soi"; r = 1;
      '-le':
         switch (w1) {
            'soi':    w = "-le à soi";
            '-lui':   w = "-le à -lui"; 
            '-leur':  w = "-le à eux";
            default:  w = "-le"; r = 1;
         }
      '-la':
         switch (w1) {
            'soi':    w = "-la à soi";
            '-lui':   w = "-la à -lui"; 
            '-leur':  w = "-la à eux";
            default:  w = "-la"; r = 1;
         }
      '-les':
         switch (w1) {
            'soi':    w = "-les à soi";
            '-lui':   w = "-les à -lui"; 
            '-leur':  w = "-les à eux";
            '-y':     w = "-les dans -y"; 
            default:  w = "-les"; r = 1;
         }
      '-lui':
         switch (w1) {
            '-en':    w = "-en à -lui";
            default:  w = "à -lui"; r = 1;
         }  
      '-leur':
         switch (w1) {
            '-en':    w = "-en à eux";
            default:  w = "à eux"; r = 1;
         }
      '-y': w = "à -y"; r = 1;

      default: r = 0;
   }
   
   if (r) {
      if (r == 1) {
         EffaceMot(p+1);
         InsertMot(p+1, w);
      }
      else {
         EffaceMot(p+1);
         EffaceMot(p+2);
         InsertMot(p+1, w);
      }
   }
   
   .jlang;
   Tokenise__(buffer, parse);
];

[ PronounsSub x y c d;
   L__M(##Pronouns, 1);
   c = (LanguagePronouns-->0)/3;
   if (player ~= selfobj) c++;

   if (c == 0) return L__M(##Pronouns, 4);

   for (x = 1, d = 0 : x<=LanguagePronouns-->0 : x = x + 3) {
      LanguagePronom(LanguagePronouns-->x);
      y = LanguagePronouns-->(x+2);
      if (y == NULL) L__M(##Pronouns, 3);
      else {
         L__M(##Pronouns, 2);
         print (the) y;
      }
      d++;
      if (d < c-1) print (string) " ; ";
      if (d == c-1) print (SerialComma) c, (string) AND__TX;
   }
   if (player ~= selfobj) {
      print (address) ME1__WD; L__M(##Pronouns, 2);
      c = player; player = selfobj;
      print (the) c; player = c;
   }
   L__M(##Pronouns, 5);
];

