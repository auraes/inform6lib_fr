! ==============================================================================
!   ENGLISH:  Language Definition File
!
!   Supplied for use with Inform 6 -- Release 6.12.6 -- Serial number 220219
!
!   Copyright Graham Nelson 1993-2004 and David Griffith 2012-2022
!
!   This code is licensed under either the traditional Inform license as
!   described by the DM4 or the Artistic License version 2.0.  See the
!   file COPYING in the distribution archive.
!
!   This file is automatically Included in your game file by "parserm".
!   Strictly, "parserm" includes the file named in the "language__" variable,
!   whose contents can be defined by+language_name=XXX compiler setting (with a
!   default of "english").
!
!   Define the constant DIALECT_US before including "Parser" to obtain American
!   English.
! ==============================================================================
! Lionel Ange, 2008-2022 pour la traduction et l'adaptation en français.
! Version 1.1.0
! Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0) 
! https://creativecommons.org/licenses/by-sa/4.0/deed.fr
! ==============================================================================
! Source encoding UTF-8 No Mark

Replace PronounsSub;

System_file;
Constant LanguageVersion = "Adaptation de la biblioth@`eque logicielle 6.12 en 
fran@ccais par Lionel Ange";

#Ifndef LIBRARY_FRENCH; ! if this file is already included,
            ! don't try to include it again.

! ------------------------------------------------------------------------------
!   Part I.   Preliminaries
! ------------------------------------------------------------------------------

! Constant EnglishNaturalLanguage;    ! Needed to keep old pronouns mechanism

! treize emplacements disponibles ; il manque 'ä' 'ü' 'ÿ' 'ö' 'æ'
#Ifdef TARGET_ZCODE;
Zcharacter 'à';
Zcharacter 'â';
Zcharacter 'ç';
Zcharacter 'è';
Zcharacter 'é';
Zcharacter 'ê';
Zcharacter 'ë';
Zcharacter 'î';
Zcharacter 'ï';
Zcharacter 'ô';
Zcharacter 'œ';
Zcharacter 'ù';
Zcharacter 'û';
#Endif;

Class CompassDirection
   with number 0, articles "Le " "le " "au ",
      description [;
         if (location provides compass_look && location.compass_look(self)) rtrue;
         if (self.compass_look()) rtrue;
         L__M(##Look, 7, self);
      ],
      compass_look false,
      parse_name [; return -1; ]
   has scenery;

Object Compass "compass" has concealed;

#Ifndef WITHOUT_DIRECTIONS;
CompassDirection -> n_obj
   with short_name "nord",
      door_dir n_to,
      name 'n//' 'nord';

CompassDirection -> s_obj
   with short_name "sud",
      door_dir s_to,
      name 's//' 'sud';

CompassDirection -> e_obj
   with short_name "est",
      door_dir e_to,
      name 'e//' 'est',
      articles "L'" "l'" "à l'";

CompassDirection -> w_obj
   with short_name "ouest",
      door_dir w_to,
      name 'o//' 'ouest',
      articles "L'" "l'" "à l'";

CompassDirection -> ne_obj
   with short_name "nord-est",
      door_dir ne_to,
      name 'ne' 'nord-est';

CompassDirection -> nw_obj
   with short_name "nord-ouest",
      door_dir nw_to,
      name 'no' 'nord-ouest';
   
CompassDirection -> se_obj
   with short_name "sud-est",
      door_dir se_to,
      name 'se' 'sud-est';

CompassDirection -> sw_obj
   with short_name "sud-ouest",
      door_dir sw_to,
      name 'so' 'sud-ouest';

CompassDirection -> u_obj
   with short_name "haut",
      door_dir u_to,
      name 'm//' 'haut', ! ceiling above sky
      articles "Le " "le " "en ";

CompassDirection -> d_obj
   with short_name "bas",
      door_dir d_to,
      name 'd//' 'bas', ! floor below ground
      articles "Le " "le " "en ";
      
#Endif; ! WITHOUT_DIRECTIONS

CompassDirection -> in_obj
   with short_name "intérieur",
      door_dir in_to,
      name 'intérieur',
      articles "L'" "l'" "à l'";

CompassDirection -> out_obj
   with short_name "extérieur",
      door_dir out_to,
      name 'extérieur',
      articles "L'" "l'" "à l'";

! ------------------------------------------------------------------------------
!   Part II.   Vocabulary
! ------------------------------------------------------------------------------

Constant AGAIN1__WD     = 'encore';
Constant AGAIN2__WD     = 'g//';
Constant AGAIN3__WD     = 'encore';

Constant OOPS1__WD      = 'oups';
Constant OOPS2__WD      = 'oups';
Constant OOPS3__WD      = 'oups';

Constant UNDO1__WD      = 'annuler'; ! undo
Constant UNDO2__WD      = 'annuler';
Constant UNDO3__WD      = 'annuler';

Constant ALL1__WD       = 'tout';
Constant ALL2__WD       = 'tous';
Constant ALL3__WD       = 'toutes';
Constant ALL4__WD       = 'tout';
Constant ALL5__WD       = 'tout';

Constant AND1__WD       = 'et';
Constant AND2__WD       = 'et';
Constant AND3__WD       = 'et';

Constant BUT1__WD       = 'sauf'; ! mais pas
Constant BUT2__WD       = 'sauf';
Constant BUT3__WD       = 'sauf';

Constant ME1__WD        = 'soi';
Constant ME2__WD        = 'moi';
Constant ME3__WD        = 'toi';

Constant OF1__WD        = 'de'; ! TODO all of
Constant OF2__WD        = 'du';
Constant OF3__WD        = 'd^';
Constant OF4__WD        = 'de';

Constant OTHER1__WD     = 'autre'; ! un une/des_autres encore
Constant OTHER2__WD     = 'autres';
Constant OTHER3__WD     = 'autre';

Constant THEN1__WD      = 'puis'; ! et
Constant THEN2__WD      = 'ensuite';
Constant THEN3__WD      = 'puis';

Constant NO1__WD        = 'n//';
Constant NO2__WD        = 'non';
Constant NO3__WD        = 'non';

Constant YES1__WD       = 'o//';
Constant YES2__WD       = 'oui';
Constant YES3__WD       = 'oui';

Constant AMUSING__WD    = 'bonus';  ! amusing

Constant FULLSCORE1__WD = 'total';  ! fullscore complet
Constant FULLSCORE2__WD = 'scores'; ! full

Constant QUIT1__WD      = 'q//';
Constant QUIT2__WD      = 'quitter';

Constant RESTART__WD    = 'recommencer';

Constant RESTORE__WD    = 'charger';


Array LanguagePronouns table

   ! word      possible GNAs                   connected
   !           to follow:                      to:
   !           a     i
   !           s  p  s  p
   !           mfnmfnmfnmfn 

!  WARNING : conflit l' le la les -> animate/inanimate
!  '-le'     $$000000100000                    NULL
!  '-la'     $$000000010000                    NULL
!  '-l^'     $$000000110000                    NULL
!  '-les'    $$000000000110                    NULL

   '-le'     $$100000100000                    NULL
   '-la'     $$010000010000                    NULL
   '-l^'     $$110000110000                    NULL
   '-les'    $$000110000110                    NULL

   'lui'     $$100000000000                    NULL
   'elle'    $$010000000000                    NULL
   'eux'     $$000110000000                    NULL
   'elles'   $$000010000000                    NULL
   '-lui'    $$110000000000                    NULL
   '-leur'   $$000110000000                    NULL
!  '-en'     $$110110110110                    NULL
   '-en'     $$000000110110                    NULL
   '-y'      $$000000110110                    NULL
 
!  'ca'      $$000000110110                    NULL ! ceci cela
!   celui celle ceux celles
!  'dessus'  $$000000110110                    NULL ! sur dessus
!  'dedans'  $$000000110110                    NULL ! dans dedans
!  'dessous' $$000000110110                    NULL ! sous dessous
;

Array LanguageDescriptors table

   ! word      possible GNAs   descriptor      connected
   !           to follow:      type:           to:
   !           a     i
   !           s  p  s  p
   !           mfnmfnmfnmfn

   'mon'     $$100000100000    POSSESS_PK      0
   'ma'      $$010000010000    POSSESS_PK      0
   'mes'     $$000110000110    POSSESS_PK      0
   'ton'     $$100000100000    POSSESS_PK      0
   'ta'      $$010000010000    POSSESS_PK      0
   'tes'     $$000110000110    POSSESS_PK      0

!  'ce'      $$100000100000    POSSESS_PK      1 ! cet cette ces

!  'notre'   $$110000110000    POSSESS_PK      0
!  'nos'     $$000110000110    POSSESS_PK      0
!  'votre'   $$110000110000    POSSESS_PK      0
!  'vos'     $$000110000110    POSSESS_PK      0

   'son'     $$100000100000    POSSESS_PK      'lui'
   'sa'      $$010000010000    POSSESS_PK      'lui'
   'ses'     $$000110000110    POSSESS_PK      'lui'
   'son'     $$100000100000    POSSESS_PK      'elle'
   'sa'      $$010000010000    POSSESS_PK      'elle'
   'ses'     $$000110000110    POSSESS_PK      'elle'
   'leur'    $$110000110000    POSSESS_PK      'eux'
   'leurs'   $$000110000110    POSSESS_PK      'eux'
   'leur'    $$110000110000    POSSESS_PK      'elles'
   'leurs'   $$000110000110    POSSESS_PK      'elles'
!   sien sienne siens siennes

   'le'      $$100000100000    DEFART_PK       NULL !/* (the) (The)
   'la'      $$010000010000    DEFART_PK       NULL
   'l^'      $$110000110000    DEFART_PK       NULL
   'les'     $$000110000110    DEFART_PK       NULL !*/

   'un'      $$100000100000    INDEFART_PK     NULL !/* (a) (A)
   'une'     $$010000010000    INDEFART_PK     NULL
   'des'     $$000110000110    INDEFART_PK     NULL !*/ (aussi préposition)

   'd^'      $$000000000000    INDEFART_PK     NULL
   'de'      $$000000000000    INDEFART_PK     NULL
   'du'      $$000000000000    INDEFART_PK     NULL
!  'des'     $$000000000000    INDEFART_PK     NULL

!  'lit'     $$111111111111    light           NULL ! adjectifs épithètes
!  'lighted' $$111111111111    light           NULL
!  'unlit'   $$111111111111    (-light)        NULL
;

Array LanguageNumbers table
   'un' 1 'une' 1 'deux' 2 'trois' 3 'quatre' 4 'cinq' 5
   'six' 6 'sept' 7 'huit' 8 'neuf' 9 'dix' 10
   'onze' 11 'douze' 12 'treize' 13 'quatorze' 14 'quinze' 15
   'seize' 16 'dix-sept' 17 'dix-huit' 18 'dix-neuf' 19 'vingt' 20;

! ------------------------------------------------------------------------------
!   Part III.   Translation
! ------------------------------------------------------------------------------

include "translation.h";
[ LanguageToInformese;
   Translation();
];

! ------------------------------------------------------------------------------
!   Part IV.   Printing
! ------------------------------------------------------------------------------

Constant LanguageAnimateGender = male;
Constant LanguageInanimateGender = male;

Constant LanguageContractionForms = 2;     ! English has two:
                                           ! 0 = starting with a consonant
                                           ! 1 = starting with a vowel

[ LanguageContraction text; ! ï ÿ Ÿ æ Æ è È
   if (text->0 == 
         'a' or 'e' or 'h' or 'i' or 'o' or 'u' or
         'â' or 'é' or 'ê' or 'î' or 'ô' or 'œ' or 
         'A' or 'E' or 'H' or 'I' or 'O' or 'U' or
         'Â' or 'É' or 'Ê' or 'Î' or 'Ô' or 'Œ')
      rtrue;
   rfalse;
];

Array LanguageArticles -->

 !   Contraction form 0:     Contraction form 1:
 !   Cdef   Def    Indef     Cdef   Def    Indef

     "Le "  "le "  "un "     "L'"   "l'"   "un "          ! Articles 0
     "La "  "la "  "une "    "L'"   "l'"   "une "         ! Articles 1
     "Les " "les " "des "    "Les " "les " "des ";        ! Articles 2

                   !             a           i
                   !             s     p     s     p
                   !             m f n m f n m f n m f n

Array LanguageGNAsToArticles --> 0 1 0 2 2 2 0 1 0 2 2 2;

[ LanguageDirection d;
   switch (d) {
      n_to:    print "au nord";
      s_to:    print "au sud";
      e_to:    print "à l'est";
      w_to:    print "à l'ouest";
      ne_to:   print "au nord-est";
      nw_to:   print "au nord-ouest";
      se_to:   print "au sud-est";
      sw_to:   print "au sud-ouest";
      u_to:    print "en haut";
      d_to:    print "en bas";
      in_to:   print "à l'intérieur";
      out_to:  print "à l'extérieur";
      default: return RunTimeError(9,d);
   }
];

[ LanguageNumber n; ! -32768 <= n <= +32767
   if (n == 0) { print "zéro"; rfalse; }
   if (n < 0) { print "moins "; n=-n; }
   if (n >= 1000) {
      if (n >= 2000){ print (LanguageNumber) n/1000; print " "; }
      print "mille";
      n = n%1000;
      if (n) print " ";
   }
   if (n >= 100) {
      if (n >= 200){ print (LanguageNumber) n/100; print " "; }
      print "cent";
      if (n == 100) rfalse;
      n = n%100;
      if (n) print " ";
      else print "s";
   }
   if (n==0) rfalse;
   switch(n)
   {
      1:  print "un"; ! une
      2:  print "deux";
      3:  print "trois";
      4:  print "quatre";
      5:  print "cinq";
      6:  print "six";
      7:  print "sept";
      8:  print "huit";
      9:  print "neuf";
      10: print "dix";
      11: print "onze";
      12: print "douze";
      13: print "treize";
      14: print "quatorze";
      15: print "quinze";
      16: print "seize";
      17: print "dix-sept";
      18: print "dix-huit";
      19: print "dix-neuf";
      20: print "vingt";
      30: print "trente";
      40: print "quarante";
      50: print "cinquante";
      60: print "soixante";
      70: print "soixante-dix";
      71: print "soixante et onze";
      72 to 79: print "soixante-", (LanguageNumber) 10 + n%10;
      80: print "quatre-vingts";
      81 to 89: print "quatre-vingt-", (LanguageNumber) n%10;
      90 to 99: print "quatre-vingt-", (LanguageNumber) 10 + n%10;
      default:
         print (LanguageNumber) n - n%10;
         if (n%10 == 1) print " et ";
         else print "-";
         print (LanguageNumber) n%10;
   }
];

[ LanguageTimeOfDay hours mins;
   if (hours < 10) print " ";
   else print hours/10;
   print hours%10, " h";
   if (mins > 0) print " ", mins/10, mins%10;
];

! Pour les mots tronqués par le dictionnaire et/ou avec accents
! sud-oues|t nord-oue|st intérieu|r extérieu|r ?
! PrintVerb() est disponible pour ajouter des verbes dans le code source du jeu.

[ LanguageVerb i no_caps;
   switch (i) {
!     'a//':           Cap("aller", no_caps);
      'd//':           Cap("descendre", no_caps);
      'g//':           Cap("encore", no_caps);
      'i//':           Cap("inventaire", no_caps);
      'l//', 'r//':    Cap("regarder", no_caps);
      'm//':           Cap("monter", no_caps);
      'x//':           Cap("examiner", no_caps);
      'z//':           Cap("attendre", no_caps);
      'déverrouiller': Cap("déverrouiller", no_caps);
      'enregistrer':   Cap("enregistrer", no_caps);
      'interroger':    Cap("interroger", no_caps);
      'introduire':    Cap("introduire", no_caps);
      'rechercher':    Cap("rechercher", no_caps);
      'recommencer':   Cap("recommencer", no_caps);
      'réfléchir':     Cap("réfléchir", no_caps);
      'répondre':      Cap("répondre", no_caps);
      'réveiller':     Cap("réveiller", no_caps);
      'sauvegarder':   Cap("sauvegarder", no_caps);
      'transcription': Cap("transcription", no_caps);
      'verrouiller':   Cap("verrouiller", no_caps);
! LanguageCommand()
      'n//':           Cap("nord", no_caps);
      's//':           Cap("sud", no_caps);
      'e//':           Cap("est", no_caps);
      'o//':           Cap("ouest", no_caps);
      'ne':            Cap("nord-est", no_caps);
      'no':            Cap("nord-ouest", no_caps);
      'se':            Cap("sud-est", no_caps);
      'so':            Cap("sud-ouest", no_caps);
      default: rfalse;
   }
];

[ LanguagePronom i;
   switch (i) {
      '-l^':   print "l'";
      '-le':   print "le";
      '-la':   print "la";
      '-les':  print "les";
      '-lui':  print "lui";
      '-leur': print "leur";
      '-en':   print "en";
      '-y':    print "y";
      default: print (address) i;
   }
];

! ----------------------------------------------------------------------------
!  LanguageVerbIsDebugging is called by SearchScope. It should return true
!  if word w is a debugging verb which needs all objects to be in scope.
! ----------------------------------------------------------------------------

#Ifdef DEBUG;
[ LanguageVerbIsDebugging w;
   if (w == 'purloin' or 'tree' or 'abstract'
      or 'gonear' or 'scope' or 'showobj')
      rtrue;
   rfalse;
];
#Endif;

! ----------------------------------------------------------------------------
!  LanguageVerbLikesAdverb is called by PrintCommand when printing an UPTO_PE
!  error or an inference message. Words which are intransitive verbs, i.e.,
!  which require a direction name as an adverb ('walk west'), not a noun
!  ('I only understood you as far as wanting to touch /the/ ground'), should
!  cause the routine to return true.
! ----------------------------------------------------------------------------

[ LanguageVerbLikesAdverb;
!   if (w == 'look' or 'go' or 'run' or 'leave' or 'l//' or 'push' or 'walk')
!      rtrue;
   rfalse;
];

! ----------------------------------------------------------------------------
!  LanguageVerbMayBeName is called by NounDomain when dealing with the 
!  player's reply to a "Which do you mean, the short stick or the long
!  stick?" prompt from the parser. If the reply is another verb (for example,
!  LOOK) then then previous ambiguous command is discarded /unless/
!  it is one of these words which could be both a verb /and/ an
!  adjective in a 'name' property.
! ----------------------------------------------------------------------------

[ LanguageVerbMayBeName;
!  if (w == 'long' or 'short' or 'normal' or 'brief' or 'full' or 'verbose')
!     rtrue;
   rfalse;
];

Constant NKEY__TX       = "S = sujet suivant";
Constant PKEY__TX       = "P = précédent";
Constant QKEY1__TX      = "     Q = retour";
Constant QKEY2__TX      = "Q = menu précédent";
Constant RKEY__TX       = "Entrée = lire le sujet";

Constant NKEY1__KY      = 'S';
Constant NKEY2__KY      = 's';
Constant PKEY1__KY      = 'P';
Constant PKEY2__KY      = 'p';
Constant QKEY1__KY      = 'Q';
Constant QKEY2__KY      = 'q';

Constant SCORE__TX      = "Score : ";
Constant MOVES__TX      = "Tour : "; ! Tour(s) énième ?
Constant TIME__TX       = "Heure : ";
Constant SCORE_S__TX    = "S : ";
Constant MOVES_S__TX    = "T : ";
Constant TIME_S__TX     = "H : ";

Constant CANTGO__TX     = "Vous ne pouvez pas aller par là.";
Constant FORMER__TX     = "vous avant"; ! your former self
Constant MYFORMER__TX   = "moi avant";  ! my former self
Constant YOURSELF__TX   = "vous-même";  ! (name) player|(object) player=(self object)
Constant MYSELF__TX     = "moi-même";
Constant YOU__TX        = "Vous";
Constant DARKNESS__TX   = "L'obscurité";

Constant THOSET__TX     = "ces choses-là";
Constant THAT__TX       = "cela";
Constant THE__TX        = "le"; ! Class SelfClass, article THE__TX
Constant OR__TX         = " ou ";
Constant NOTHING__TX    = "rien";
Constant IS__TX         = " est";
Constant ARE__TX        = " sont";
Constant IS2__TX        = ""; ! "est "
Constant ARE2__TX       = ""; ! "sont "
Constant WAS__TX        = " was";
Constant WERE__TX       = " were";
Constant WAS2__TX       = "was ";
Constant WERE2__TX      = "were ";
Constant AND__TX        = " et ";
Constant WHOM__TX       = ""; ! whom
Constant WHICH__TX      = ""; ! which
Constant COMMA__TX      = ", ";
Constant COLON__TX      = " :";

! For EnterSub()
Constant STAND__TX      = 'lever';
Constant SIT__TX        = 'asseoir';
Constant LIE__TX        = 'allonger'; ! coucher

Constant LIBERROR__TX   = "Bibliothèque : erreur ";
Constant TERP__TX       = "Interpréteur ";
Constant VER__TX        = "Version ";
Constant STDTERP__TX    = "Interpréteur standard ";
Constant TERPVER__TX    = "Interpréteur : version ";
Constant LIBSER__TX     = "Bibliothèque : numéro de série ";
Constant VM__TX         = "VM ";
Constant RELEASE__TX    = "Version ";
Constant SERNUM__TX     = "Numérotée "; !Numéro de série 
Constant INFORMV__TX    = "Inform v";
Constant LIBRARYV__TX   = " Bibl. v"; !Bibliothèque 

[ elide w;
   PrintToBuffer(StorageForShortName, 160, w);
   return (LanguageContraction(StorageForShortName + WORDSIZE));
];

[ _nt obj;
   if (obj has pluralname) print "nt ";
   else print " ";
];

[ _s obj;
   if (obj has pluralname) print "s";
];

[ _es obj;
   if (obj has female) print "e";
   _s(obj);
];

[ _lesl obj;
   if (obj has pluralname) print " les ";
   else print " l'";
];

[ _dedudes obj nospace;
   if (nospace == false) print " ";
   if (obj has pluralname) { print "des "; return; }
   if (obj has proper) { print "de "; return; }
   if ( elide(obj) ) { print "de l'"; return; } 
   if (obj has female) print "de la ";
   else print "du ";
];

[ _aaux obj nospace;
   if (nospace == false) print " ";
   if (obj has pluralname) { print "aux "; return; }
   if (obj has proper) { print "à "; return; }
   if ( elide(obj) ) { print "à l'"; return; }
   if (obj has female) print "à la ";
   else print "au ";
];

[ ThatOrThose obj; 
   if (obj == player) print "vous";
   else print "cela";
];

[ ItorThem obj;
   print " ";
   if (obj == player) { print "vous "; return; }
   if (obj has pluralname) { print "les "; return; }
   if (obj has female) print "la ";
   else print "le ";
];

[ IsorAre obj negation;
   print " ";
   if (obj == player) {
      if (negation) print "n'";
      print "êtes ";
      return;
   } 
   if (obj has pluralname) {
      if (negation) print "ne ";
      print "sont ";
   }
   else {
      if (negation) print "n'";
      print "est ";
   }
];

[ CThatorThose obj;
   print " ";
   if (obj == player) { print "vous"; return; }
   if (obj has female) print "elle";
   else print "il";
   _s(obj);
];

[ CTheyreOrThats obj; print (The) obj, (IsorAre) obj; ];

[ Tense present past;
   if (player provides narrative_tense && player.narrative_tense == PAST_TENSE) {
      if (past == false) return;
      print (string) past;
   }
   else print (string) present;
];

[ DecideAgainst;
   "Vous décidez que ce n'est finalement pas une si bonne idée.";
];

#Ifdef TARGET_ZCODE;

[ LowerCase c; ! for ZSCII matching ISO 8859-1
   switch (c) {
      'A' to 'Z':                            c = c + 32;
      202, 204, 212, 214, 221:               c--;
      217, 218:                              c = c - 2;
      158 to 160, 167, 168, 208 to 210:      c = c - 3;
      186 to 190, 196 to 200:                c = c - 5;
      175 to 180:                            c = c - 6;
   }
   return c;
];

[ UpperCase c; ! for ZSCII matching ISO 8859-1
   switch (c) {
      'a' to 'z':                            c = c - 32;
      201, 203, 211, 213, 220:               c++;
      215, 216:                              c = c + 2;
      155 to 157, 164, 165, 205 to 207:      c = c + 3;
      181 to 185, 191 to 195:                c = c + 5;
      169 to 174:                            c = c + 6;
   }
   return c;
];

#Ifnot; ! TARGET_GLULX

[ LowerCase c; return glk_char_to_lower(c); ];
[ UpperCase c; return glk_char_to_upper(c); ];

#Endif; ! TARGET_

[ LanguageLM n x1 x2;
   Answer, Ask: "Vous n'obtenez aucune réponse.";

!  Ask: see Answer

   Attack: "La violence n'est pas toujours la solution.";

   Blow: "Cela n'aboutirait pas à grand-chose.";

   Burn: switch (n) {
      1: "Commettre un tel acte n'apporterait rien.";
      2: DecideAgainst();
   }

   Buy: "Rien n'est en vente."; ! Il n'y a rien à vendre ici.

   Climb: switch (n) {
      1: "Cela ne servirait pas à grand-chose."; ! Grimper cela
      2: DecideAgainst();
   }

   Close: switch (n) {
!     1: print (The) x1; IsorAre(x1, 1);
!        "pas quelque chose que vous pouvez fermer.";
      1: "Ce n'est pas quelque chose que vous pouvez fermer.";
      2: "", (CTheyreOrThats) x1, "déjà fermé", (_es) x1, ".";
      3: "Vous fermez ", (the) x1, ".";
      4: "(fermant d'abord ", (the) x1, ")";
   }

   CommandsOff: switch (n) {
      1: "[L'enregistrement des commandes est désactivé.]";
      #Ifdef TARGET_GLULX;
      2: "[L'enregistrement des commandes est déjà désactivé.]";
      #Endif; ! TARGET_
   }

   CommandsOn: switch (n) {
      1: "[L'enregistrement des commandes est activé.]";
      #Ifdef TARGET_GLULX;
      2: "[Rejouer les commandes est en cours.]";
      3: "[L'enregistrement des commandes est déjà activé.]";
      4: "[L'enregistrement des commandes a échoué.]";
      #Endif; ! TARGET_
   }

   CommandsRead: switch (n) {
      1: "[Rejouer les commandes.]";
      #Ifdef TARGET_GLULX;
      2: "[Rejouer les commandes est déjà en cours.]";
      3: "[Rejouer les commandes a échoué. L'enregistrement des commandes 
         est activé.]";
      4: "[Rejouer les commandes a échoué.]";
      5: "[Rejouer les commandes est terminé.]";
      #Endif; ! TARGET_
   }

   Consult: "Vous ne trouvez rien d'intéressant dans ", (the) x1, ".";

   Cut: switch (n) {
      1: "Cela ne servirait pas à grand-chose."; ! Couper cela
      2: DecideAgainst();
   }

   Dig: "Cela n'aboutirait pas à grand-chose."; ! Creuser ici

   Disrobe: switch (n) {
      1: "Vous ne", (ItorThem) x1, "portez pas sur vous.";
      2: "Vous retirez ", (the) x1, "."; ! enlevez
      3: "(retirant d'abord ", (the) x1, ")";
      4: "Vous devez d'abord retirer ", (the) x1, "."; ! enlever
   }

!  Drink: "Il n'y a rien de vraiment buvable ici.";
   Drink: "Ce n'est pas quelque chose que vous pouvez boire.";
      
   Drop: switch (n) {
      1: "", (CTheyreOrThats) x1, "déjà ici."; !FIXME "y est déjà"
      2: "Vous ne", (_lesl) x1, "avez pas sur vous.";
      3: "C'est posé.";
      4: "Vous devez avoir ", (the) x1, " avec vous avant de pouvoir", (itorthem) x1,
         "poser.";
   }

   Eat: switch (n) {
!     1: print (The) x1; IsorAre(x1, 1); "manifestement pas comestible.";
      1: "Ce n'est pas quelque chose que vous pouvez manger.";
      2: "Vous mangez ", (the) x1, ". Pas mauvais.";
   }

   EmptyT: switch (n) {
      1: print (The) x1, " ne peu"; if (x1 has pluralname) print "ven";
         "t rien contenir.";
      2: "", (CTheyreOrThats) x1, "fermé", (_es) x1, ".";
      3: "", (CTheyreOrThats) x1, "déjà vide", (_s) x1, ".";
      4: "Cela ne viderait rien du tout.";
   }

   Enter: switch (n) {
      1: print "Vous êtes déjà ";
         if (x1 has supporter) print "sur"; else print "dans";
         " ", (the) x1, ".";
      2: print "Ce n'est pas quelque chose où vous pouvez ";
         switch (x2) { ! vous tenir
            'asseoir':  "vous asseoir.";
            'coucher':  "vous coucher.";
            'allonger': "vous allonger.";
            default:    "aller.";
         }
      3: "C'est impossible car ", (the) x1, (IsorAre) x1, "fermé", (_es) x1, ".";
      4: "Vous n'arriverez à rien comme cela."; ! free-standing
      5: print "Vous ";
         if (x1 has supporter) print "allez sur"; else print "allez dans";
         " ", (the) x1, ".";
      6: if (x1 has supporter) print "(descendant"; else print "(sortant";
         " d'abord", (_dedudes) x1, (name) x1, ")";
      7: if (x1 has supporter) "(allant sur ", (the) x1, ")^";
         if (x1 has container) "(allant dans ", (the) x1, ")^";
         "(allant dans ", (the) x1, ")^"; ! vers
   }

   Examine: switch (n) {
      1: "Vous êtes dans l'obscurité, rien n'est visible.";
      2: "Vous ne remarquez rien de particulier concernant ", (the) x1, ".";
      3: print (CTheyreOrThats) x1, "actuellement ";
         if (x1 has on) "en marche."; else "arrêté", (_es) x1, "."; ! à l'arrêt
   }

   Exit: switch (n) {
      1: "Vous n'êtes dans rien pour l'instant.";
      2: "C'est impossible car ", (the) x1, (IsorAre) x1, "fermé", (_es) x1, ".";
      3: print "Vous ";
         if (x1 has supporter) print "descendez"; else print "sortez";
         "", (_dedudes) x1, (name) x1, ".";
      4: print "Vous n'êtes pas ";
         if (x1 has supporter) print "sur"; else print "dans";
         " ", (the) x1, ".";
      5: if (x1 has supporter) print "(descendant"; else print "(sortant";
         " d'abord", (_dedudes) x1, (name) x1, ")";
      6: "TODO ! Exit 6";
   }

   Fill: switch (n) {
      1: "Il n'y a rien de vraiment approprié pour remplir ", (the) x1, ".";
      2: "Remplir ", (the) x1, " avec ", (the) x2, " n'aurait aucun sens.";
   }

   FullScore: switch (n) {
      1: print "Le score ";
         if (deadflag) print "était"; else print "est"; " composé de :^";
      2: print "point";
         if (things_score > 1) print "s"; " pour les objets trouvés";
      3: print "point";
         if (places_score > 1) print "s"; " pour les lieux visités";
      4: print "point";
         if (score > 1) print "s"; " (sur un total de ", MAX_SCORE, ")";
   }

   GetOff: "Vous n'êtes pas sur ", (the) x1, " pour l'instant.";

   Give: switch (n) {
      1: "Vous n'avez pas ", (the) x1, " avec vous.";
      2: "Vous jonglez brièvement avec ", (the) x1, ".";
      3: "", (The) x1, " ne semble", (_nt) x1, "pas intéressé", (_es) x1, ".";
      4: "TODO ! Give 4";
   }

   Go: switch (n) {
      1: print "Vous devez d'abord ";
         if (x1 has supporter) print "descendre"; else print "sortir";
         "", (_dedudes) x1, (name) x1, ".";
      2: "Vous ne pouvez pas aller par là.";
      3: "Vous ne pouvez pas monter par ", (the) x1, ".";
      4: "Vous ne pouvez pas descendre par ", (the) x1, ".";
      5: "C'est impossible car ", (the) x1, (IsorAre) x1, "sur votre chemin."; ! passage
      6: "C'est impossible car ", (the) x1, " ne mène", (_nt) x1, "nulle part.";
      7: "TODO ! GO 7";
   }

   Insert: switch (n) {
      1: "Vous devez avoir ", (the) x1, " avec vous avant de pouvoir", (itorthem) x1,
         "mettre dans quelque chose d'autre.";
      2: print (The) x1, " ne peu"; if (x1 has pluralname) print "ven";
         "t rien contenir.";
      3: "", (CTheyreOrThats) x1, "fermé", (_es) x1, ".";
      4: "Vous devez d'abord", (itorthem) x1, "prendre."; ! n'est jamais appelé
      5: "Vous ne pouvez pas mettre quelque chose dans lui-même.";
      6: "** LM:Insert 6 **"; ! => Disrobe 3
      7: "Il n'y a plus suffisamment de place dans ", (the) x1, "."; !TODO ne peut rien contenir d'autre, plus/pas suffisamment ?
      8: "C'est fait.";
      9: "Vous mettez ", (the) x1, " dans ", (the) x2, ".";
   }

   Inv: switch (n) {
      1: "Vous n'avez aucun objet.";
      2: print "Vous avez";
      3: " :";
      4: ".";
   }

   Jump: "Vous sautez sur place, en vain.";

   JumpIn: switch(n) {
!     1: "Sauter dans ", (the) x1, " ne servirait pas à grand-chose.";
      1: "Cela ne servirait pas à grand-chose."; ! ne_vous à_rien
      2: DecideAgainst();
   }

   JumpOn: switch(n) {
!     1: "Sauter sur ", (the) x1, " ne servirait pas à grand-chose.";
      1: "Cela ne servirait pas à grand-chose.";
      2: DecideAgainst();
   }

   JumpOver: switch(n) {
!     1: "Sauter par-dessus ", (the) x1, " ne servirait pas à grand-chose.";
      1: "Vous n'arriverez à rien comme cela.";
      2: DecideAgainst();
   }

   Kiss: "Concentrez-vous plutôt sur le jeu.";

   Listen: "Vous n'entendez rien de particulier.";

   ListMiscellany: switch (n) {
      1:  print " (éclairé", (_es) x1, ")"; ! allumé (providing light)
      2:  print " (fermé", (_es) x1, ")";
      3:  print " (fermé", (_es) x1, " et éclairé", (_es) x1, ")";
      4:  print " (vide", (_s) x1, ")";
      5:  print " (vide", (_s) x1, " et éclairé", (_es) x1, ")";
      6:  print " (fermé", (_es) x1, " et vide", (_s) x1, ")";
      7:  print " (fermé", (_es) x1, ", vide", (_s) x1, " et éclairé", (_es) x1, ")";
      8:  print " (éclairé", (_es) x1, " et porté", (_es) x1;
      9:  print " (éclairé", (_es) x1;
      10: print " (porté", (_es) x1; ! TODO
      11: print " (";
      12: print "ouvert", (_es) x1;
      13: print "ouvert", (_es) x1, " et vide", (_s) x1;
      14: print "fermé", (_es) x1;
      15: print "fermé", (_es) x1, " et verrouillé", (_es) x1;
      16: print " et vide", (_s) x1;
      17: print " (vide", (_s) x1, ")";
      18: print " contenant ";
      19: print " (supportant ";       ! TODO
      20: print " supportant "; ! (, ) ! TODO
      21: print " (contenant ";
      22: print " contenant "; ! (, )
   }

   LMode1: print (string) Story, " est maintenant en mode de description courte";
           if (initial_lookmode == 1) print " (par défaut)";
           ". Seuls les lieux non visités sont décrits en détail ; 
           les autres sont décrits brièvement.";

   LMode2: print (string) Story, " est maintenant en mode de description longue";
           if (initial_lookmode ~= 1 or 3) print " (par défaut)";
           ". Tous les lieux sont décrits en détail.";

   LMode3: print (string) Story, " est maintenant en mode de description brève";
           if (initial_lookmode == 3) print " (par défaut)";
           ". Tous les lieux sont décrits brièvement.";

   Lock: switch (n) {
!     1: print (The) x1; IsorAre(x1, 1); 
!        "pas quelque chose que vous pouvez verrouiller.";
      1: "Ce n'est pas quelque chose que vous pouvez verrouiller.";
      2: "", (CTheyreOrThats) x1, "déjà verrouillé", (_es) x1, ".";
      3: "Vous devez d'abord fermer ", (the) x1, ".";
      4: "", (The) x1, " ne semble", (_nt) x1, "pas correspondre à la serrure.";
      5: "Vous verrouillez ", (the) x1, ".";
   }

   Look: switch (n) {
      1: print " (sur ", (the) x1, ")";
      2: print " (dans ", (the) x1, ")";
      3: print " (comme ", (object) x1, ")";
      4: print "^Sur ", (the) x1;
         WriteListFrom(child(x1),
          ENGLISH_BIT+RECURSE_BIT+PARTINV_BIT+TERSE_BIT+CONCEAL_BIT+ISARE_BIT);
         ".";
      5,6:
         if (x1 ~= location) {
            if (x1 has supporter) print "^Sur "; else print "^Dans ";
            print (the) x1, ", vous";
         }
         else print "^Vous";
         print " remarquez ";
         if (n == 5) print "aussi ";
         WriteListFrom(child(x1),
          ENGLISH_BIT+RECURSE_BIT+PARTINV_BIT+TERSE_BIT+CONCEAL_BIT+WORKFLAG_BIT);
         ".";
      7: "Vous ne voyez rien de particulier dans cette direction.";
   }

   LookUnder: switch (n) {
      1: "Mais vous êtes dans l'obscurité.";
      2: "Vous ne trouvez rien d'intéressant."; ! n'y
   }

   Mild: "Assez.";

   Miscellany: switch (n) {
      1: "(considérant les seize premiers objets seulement)^";
      2: "Il n'y a rien à faire.";
      3: print " Vous êtes mort ";
      4: print " Vous avez gagné ";
      5: print "^Voulez-vous recommencer, charger une partie sauvegardée";
         #Ifdef DEATH_MENTION_UNDO;
         print ", annuler votre dernière action";
         #Endif;
         if (TASKS_PROVIDED == 0) print ", voir votre score total ";
         if (deadflag == 2 && AMUSING_PROVIDED == 0)
            print ", voir quelques actions bonus amusantes";
         " ou quitter ?";
      6: "[Votre interpréteur ne permet pas d'annuler. Désolé !]";
         #Ifdef TARGET_ZCODE;
      7: "Annuler a échoué. [Certains interpréteurs ne le permettent pas.]";
         #Ifnot; ! TARGET_GLULX
      7: "[Vous ne pouvez pas annuler au delà.]";
         #Endif; ! TARGET_
      8: "Veuillez choisir parmi les propositions ci-dessus.";
      9: "^Vous êtes à présent plongé dans l'obscurité."; ! Il fait nuit noire ici.
      10: "Je vous demande pardon ?";
      11: "[Il n'y a aucune action à annuler.]";
      12: "[Il est impossible d'annuler deux fois de suite. Désolé.]";
      13: "[L'action précédente a été annulée.]";
      14: "Désolé, il est impossible de corriger.";
      15: "N'y pensez même pas.";
      16: "Oups, ne peut corriger qu'un seul mot à la fois.";
      17: "Il fait nuit noire ici, et vous n'y voyez rien.";
      18: print "vous-même";
      19: print "Aussi ";
          if (player has female) print "belle"; else print "beau";
          " que d'habitude.";
      20: "Pour répéter une commande, dites simplement Encore.";
      21: "Vous ne pouvez pas répéter cela.";
      22: "Vous ne pouvez pas commencer par un signe de ponctuation.";
      23: "Vous semblez vouloir vous adresser à quelqu'un, mais je ne vois pas à qui.";
      24: "Ce n'est pas quelque chose auquel vous pouvez vous adresser.";
      25: "Pour vous adresser à quelqu'un (ex. Lucie), essayez « Lucie, salut » 
          ou quelque chose d'approchant.";
      26: "(prenant d'abord ", (the) x1, ")";
      27: "Je n'ai pas compris cette phrase.";
      28: print "Je n'ai pris en compte que « ";
!         Je ne vous ai compris que dans la mesure où vous vouliez
      29: "Je n'ai pas compris ce nombre.";
      30: "Vous ne pouvez voir ou interagir avec cela.";
      31: "Vous semblez en avoir dit trop peu.";
      32: "Vous n'avez pas cela avec vous.";
      33: "Vous ne pouvez pas agir sur plusieurs objets avec ce verbe.";
      34: "Vous ne pouvez agir sur plusieurs objets qu'une seule fois par phrase.";
      35: "Je ne sais pas à quoi « ", (LanguagePronom) x1, " » se réfère.";
      36: "Vous tentez d'exclure quelque chose qui n'y est pas de toute façon.";
      37: "Ce n'est possible qu'avec un être ou une chose animée.";
      38: "Ce n'est pas un verbe que je reconnais.";
      39: "Ce n'est pas quelque chose auquel vous pouvez vous référer 
          au cours du jeu."; ! mentionner/évoquer 
      40: "Vous ne pouvez pas voir « ", (LanguagePronom) x1, " » (", (the) x2,
         ") pour l'instant.";
      41: "Je n'ai pas compris la fin de la phrase.";
      42: if (x1 == 0) print "Il n'y en a plus";
          else print "Il en reste seulement ", (number) x1; ! TODO une
          print " de disponible"; if (x1 > 1) print "s";
          ".";
      43: "Il n'y a rien à faire.";
      44: print "Il n'y a rien à ";
          if ( LanguageVerb(x1) == 0 ) print (address) x1;
          ".";
      45: print "Voulez-vous dire "; ! Vouliez-vous
      46: print "Voulez-vous dire "; ! Vouliez-vous
      47: "Désolé, vous ne pouvez avoir qu'un seul objet ici. Lequel exactement ?";
      48: PronomInterrogatif(); " ?";
      49: PronomInterrogatif(); " ?";
      50: print "Votre score vient ";
          if (x1 > 0) print "d'augmenter"; else { x1 = -x1; print "de diminuer"; }
          print " de ", (number) x1, " point"; ! d'un
          if (x1 > 1) print "s";
      51: "(votre liste de commandes a été réduite.)"; !?
      52: "^Saisissez un nombre entre 1 est ", x1, ", 0 pour ré-afficher ou 
          appuyez sur Entrée.";
      53: "^[Appuyez sur Espace, s'il vous plaît.]";
      54: "[Le commentaire est enregistré.]";
      55: "[Le commentaire n'a pas été enregistré.]";
      56: " »."; ! Miscellany 28
      57: " ?";  ! Miscellany 45/46
      58: print "(prenant d'abord ", (the) x1, " ";
          if (x2 has supporter) print "sur"; else print "dans";
          " ", (the) x2, ")";
      59: "Veuillez être plus précis.";
      60: "TODO ! M60"; ! print (The) x1, " observes that ";
   }

   No,Yes: "Ni oui ni non."; ! That was a rhetorical question.

   NotifyOff: "La notification du score est désactivée.";

   NotifyOn: "La notification du score est activée.";

   Objects: switch (n) {
      1:  print "Vous avez acquis";
      2:  "Vous n'avez acquis aucun objet.";
      3:  print " (sur vous)";
      4:  print " (dans l'inventaire)"; ! avec vous 
      5:  print " (donné", (_es) x2, ")"; ! cédé
      6:  print " (", (name) x1, ")";
      7:  print " (dans ", (the) x1, ")";
      8:  print " (dans ", (the) x1, ")";
      9:  print " (sur ", (the) x1, ")";
      10: print " (égaré", (_es) x2, ")"; ! perdu, hors_jeu
   }

   Open: switch (n) {
!     1: print (The) x1; IsorAre(x1, 1);
!        "pas quelque chose que vous pouvez ouvrir.";
      1: "Ce n'est pas quelque chose que vous pouvez ouvrir.";
      2: "", (The) x1, " semble", (_nt) x1, "être verrouillé", (_es) x1, ".";
      3: "", (CTheyreOrThats) x1, "déjà ouvert", (_es) x1, ".";
      4: print "Vous ouvrez ", (the) x1, ", révélant ";
         if (WriteListFrom(child(x1), ENGLISH_BIT+TERSE_BIT+CONCEAL_BIT) == 0)
            "rien du tout.";
         ".";
      5: "Vous ouvrez ", (the) x1, ".";
      6: "(ouvrant d'abord ", (the) x1, ")";
   }

   Order:
      print (The) x1;
      if (x1 has pluralname) print " ont"; else print " a";
      " mieux à faire.";

   Places: switch (n) {
      1: print "Vous avez visité";
      2: ".";
      3: "Vous n'avez visité aucun lieu.";
   }

   Pray: "Votre prière semble sans effet.";

   Prompt: print "^>";

   Pronouns: switch (n) {
      1: print "À cet instant, ";
      2: print " signifie ";
      3: print " n'est pas défini";
      4: "aucun pronom n'est attribué.";
      5: ".";
   }

   Pull, Push, Turn: switch (n) {
      1: "Vous punir de cette façon n'arrangera pas les choses.";
      2: "", (CTheyreOrThats) x1, "fixé", (_es) x1, " sur place.";
!     2: "Ce n'est pas quelque chose que vous pouvez déplacer.";
      3: "Vous ne pouvez pas faire cela.";
      4: "Vous n'aboutirez à rien comme cela.";
      5: "Ce ne serait pas très correct.";
      6: DecideAgainst();
   }

!  Push: see Pull.

   PushDir: switch (n) {
      1: "Cela n'aboutirait pas à grand-chose."; ! Cela ne servirait à rien.
      2: "Ce n'est pas une direction valide.";
      3: "Ce n'est pas une destination possible.";
   }

   PutOn: switch (n) {
      1: "Vous devez avoir ", (the) x1, " avec vous avant de pouvoir", (itorthem) x1,
         "mettre sur quelque chose d'autre.";
      2: "Vous ne pouvez pas poser quelque chose sur lui-même.";
      3: "Mettre quelque chose sur ", (the) x1, " ne servirait à rien.";
      4: "Vous semblez manquer de dextérité.";
      5: "** LM:PutOn 5 **"; ! => Disrobe 3
      6: "Il n'y a plus de place sur ", (the) x1, ".";
      7: "C'est fait.";
      8: "Vous mettez ", (the) x1, " sur ", (the) x2, ".";
   }

   Quit: switch (n) {
      1: print "Répondez, s'il vous plaît, par oui ou non "; ! par non
      2: print "Êtes-vous sûr de vouloir quitter le jeu ? ";
   }

   Remove: switch (n) {
      1: "", (CTheyreOrThats) x1, "malencontreusement fermé", (_es) x1, ".";
      2: "", (The) x1, " n'y", (IsorAre) x1, "pas pour l'instant.";
      3: "C'est retiré.";
      4: print "Mais ", (the) x1; IsorAre(x1, 1);
      "pas dans ou sur quoi que ce soit.";
   }

   Restart: switch (n) { 
      1: print "Êtes-vous sûr de vouloir recommencer ? ";
      2: "Recommencer a échoué.";
   }

   Restore: switch (n) {
      1: "Le chargement de la sauvegarde a échoué.";
      2: "C'est chargé.";
   }

   Rub: switch (n) {
      1: "Cela ne servirait pas à grand-chose.";
      2: DecideAgainst();
   }
   
   RunTimeError: print "** ";
      switch (n) {
         1:  print "Préposition non trouvée (cela ne devrait pas se produire)";
         2:  print "La valeur de la propriété n'est pas une routine ou une chaîne 
             de caractères : ~", (property) x2, "~ de ~", (name) x1, "~ (", x1, ")";
         3:  print "L'entrée dans la liste des propriétés n'est pas une routine ou 
             une chaîne de caractères : ~", (property) x2, "~ liste de ~", 
             (name) x1, "~ (", x1, ")";
         4:  print "Trop de routines timers/daemons sont actives simultanément. 
             La limite est la constante de bibliothèque MAX_TIMERS 
             (actuellement ", MAX_TIMERS, ") et devrait être augmentée";
         5:  print "Objet ~", (name) x1, "~ n'a pas la propriété ~", 
             (property) x2, "~";
         7:  print "L'objet ~", (name) x1, "~ ne peut être utilisé comme 
             objet-joueur que s'il possède la propriété ~number~";
         8:  print "Tentative d'entrée aléatoire à partir d'un tableau de 
             table vide";
         9:  print x1, " n'est pas un numéro de propriété de direction valide";
         10: print "L'objet-joueur se trouve en dehors de l'arborescence 
             d'objets.";
         11: print "Le lieu ~", (name) x1, "~ n'a pas de propriété ~", 
             (property) x2,"~";
         12: print "Tentative de définir un pronom inexistant avec SetPronoun()";
         13: print "Un mot clé 'token' ne peut être suivi que d'une préposition";
         14: print "Limite de débordement de la mémoire tampon de ", 
             x1, " utilisant '@@64output_stream 3' ", (string) x2;
         15: print "LoopWithinObject a été interrompu parce que l'objet ",
             (name) x1, " a été déplacé pendant que la boucle passait par lui";
         16: print "Tentative illégale d'utilisation de narrative_voice de ", x1;
      default:
      print "(inexpliqué)";
   }
   print " **";

   Save: switch (n) {
      1: "L'enregistrement de la sauvegarde a échoué.";
      2: "C'est enregistré.";
   }

   Score: switch (n) {
      1: if (deadflag) print "Votre score final est de ";
         else print "Votre score est de ";
         print score, " sur ", MAX_SCORE, ", en ", turns, " tour";
         if (turns > 1) print "s";
         return;
      2: "Il n'y a pas de score dans ce jeu.";
   }

   ScriptOff: switch (n) {
      1: "Aucune transcription n'est en cours.";
      2: "^Fin de la transcription.";
      3: "Impossible de terminer la transcription.";
   }

   ScriptOn: switch (n) {
      1: "Une transcription est déjà en cours.";
      2: print "Début de la transcription de "; VersionSub();
      3: "La tentative de commencer la transcription a échoué.";
   }

   Search: switch (n) {
      1: "Mais vous êtes dans l'obscurité.";
      2: "Il n'y a rien sur ", (the) x1, ".";
      3: print "Sur ", (the) x1, ", vous voyez ";
         WriteListFrom(child(x1), ENGLISH_BIT+TERSE_BIT+CONCEAL_BIT);
         ".";
      4: "Vous ne trouvez rien d'intéressant.";
      5: "C'est impossible car ", (the) x1, (IsorAre) x1, "fermé", (_es) x1, ".";
      6: "", (CTheyreOrThats) x1, "vide", (_s) x1, ".";
      7: print "Dans ", (the) x1, ", vous voyez ";
         WriteListFrom(child(x1), ENGLISH_BIT+TERSE_BIT+CONCEAL_BIT);
         ".";
   }

   Set: "Ce n'est pas quelque chose que vous pouvez régler.";

   SetTo: "Vous ne pouvez pas", (ItorThem) x1, "régler sur quoi que ce soit.";

   Show: switch (n) {
      1: "Vous n'avez pas ", (the) x1, " avec vous."; ! transportez
      2: "", (CTheyreOrThats) x1, "peu impressionné", (_es) x1, ".";
   }

   Sing:  "Vous fredonnez un air qui vous est familier.";

   Sleep: "Vous ne vous sentez pas particulièrement fatigué.";

   Smell: switch (n) {
      1: "Vous ne sentez rien de particulier.";
      2: DecideAgainst();
   }

   Sorry: "Hé, n'en faites pas trop.";

!  Squeeze remplace Push pour appuyer_sur
   Squeeze: switch (n) {
      1: DecideAgainst();
      2: "Il ne se passe rien de particulier."; ! Vous n'arriverez à rien comme cela.
   }

   Strong: "Les vrais aventuriers n'utilisent pas un tel langage.";

   Swim: "Il n'y a pas vraiment d'endroit où nager."; ! nulle part où nager

   Swing: "Il n'y a rien de vraiment approprié.";

   SwitchOff: switch (n) { ! TODO
!     1: print (The) x1; IsorAre(x1, 1);
!        "pas quelque chose que vous pouvez éteindre.";
      1: "Ce n'est pas quelque chose que vous pouvez éteindre."; ! arrêter
      2: "", (CTheyreOrThats) x1, "déjà éteint", (_es) x1, "."; ! à l'arrêt
      3: "Vous éteignez ", (the) x1, "."; ! arrêtez
   }

   SwitchOn: switch (n) { ! TODO
!     1: print (The) x1; IsorAre(x1, 1);
!        "pas quelque chose que vous pouvez allumer.";
      1: "Ce n'est pas quelque chose que vous pouvez allumer."; ! mettre en marche
      2: "", (CTheyreOrThats) x1, "déjà allumé", (_es) x1, "."; ! en marche
      3: "Vous allumez ", (the) x1, "."; ! mettez en marche
   }

   Take: switch (n) {
      1: "C'est pris.";
      2: "Vous vous êtes toujours appartenu.";
      3: print "Je doute que ", (the) x1, " soi";
         if (x1 has pluralname) print "en";
         "t d'accord avec cela.";
      4: print "Vous devez d'abord ";
         if (x1 has supporter) print "descendre"; else print "sortir";
         "", (_dedudes) x1, (name) x1, ".";
      5: "Vous", (_lesl) x1, "avez déjà avec vous.";
      6: "", (The) x2, " semble", (_nt) x2, "appartenir", (_aaux) x1, 
         (name) x1, ".";
      7: "", (The) x2, " semble", (_nt) x2, "faire partie", (_dedudes) x1, 
         (name) x1, ".";
      8: print (The) x1; IsorAre(x1, 1); "pas accessible", (_s) x1, ".";
      9: print (The) x1; IsorAre(x1, 1); "pas ouvert", (_es) x1, ".";
      10: "", (CTheyreOrThats) x1, "difficilement transportable", (_s) x1, ".";
!     11: "", (CTheyreOrThats) x1, "fixé", (_es) x1, " sur place.";
      11: "Ce n'est pas quelque chose que vous pouvez prendre.";
      12: "Vous avez déjà suffisamment d'objets.";
      13: "(mettant ", (the) x1, " dans ", (the) x2, " pour faire de la place)";
   }

   Taste: switch (n) {
      1: print "Vous ne ";
         if (x1 has pluralname) print "leur"; else print "lui";
         " trouvez aucun goût particulier.";
      2: DecideAgainst();
   }

   Tell: switch (n) {
      1: "Vous vous dites quelques mots.";
      2: "Cela ne provoque aucune réaction.";
   }

   Think: "Quelle bonne idée.";

   ThrowAt: switch (n) {
      1: "C'est sans grand intérêt.";
      2: "Vous renoncez au dernier moment.";
   }

   Tie: switch (n) {
      1: "Vous n'arriverez à rien comme cela.";
      2: DecideAgainst();
   }

   Touch: switch (n) {
      1: DecideAgainst();
      2: "Vous ne ressentez rien de particulier.";
      3: "Si vous pensez que cela peut aider.";
   }

!  Turn: see Pull.

   Unlock: switch (n) {
!     1: print (The) x1; IsorAre(x1, 1);
!        "pas quelque chose que vous pouvez déverrouiller.";
      1: "Ce n'est pas quelque chose que vous pouvez déverrouiller.";
      2: "", (CTheyreOrThats) x1, "déjà déverrouillé", (_es) x1, ".";
      3: "", (The) x1, " ne semble", (_nt) x1, "pas correspondre à la serrure.";
      4: "Vous déverrouillez ", (the) x1, ".";
      5: "(déverrouillant d'abord ", (the) x1, ")";
   }

   VagueGo: "Vous devez préciser la direction vers laquelle aller.";

   VagueVerb: "Utilisez un verbe moins générique.";

   Verify: switch (n) {
      1: "Le fichier du jeu est valide.";
      2: "Le fichier du jeu ne semble pas valide et peut être corrompu.";
   }

   Wait: "Le temps passe."; ! Un peu plus tard

   Wake: "Ce n'est pourtant pas un rêve.";

   WakeOther: "Cela ne semble pas nécessaire.";

   Wave: switch (n) {
      1: "Vous ne", (_lesl) x1, "avez pas avec vous.";
      2: print "Vous avez l'air un peu ridicule à ";
         if (x2)
            "brandir ", (the) x1, (_aaux) x2, (the) x2, ".";
         "agiter ", (the) x1, ".";
      3: DecideAgainst();
   }

   WaveHands:
      print "Vous faites signe"; ! tentez d'attirer l'attention
      switch (n) {
         1: print " aux alentours";
         2: print (_aaux) x1, (the) x1;
      }
      ", en vain.";
   Wear: switch (n) {
      1: "Vous ne pouvez pas mettre cela.";
      2: "Vous ne", (_lesl) x1, "avez pas avec vous.";
      3: "Vous", (_lesl) x1, "avez déjà sur vous.";
      4: "Vous mettez ", (the) x1, ".";
      5: "Vous devez avoir ", (the) x1, " avec vous avant de pouvoir", (itorthem) x1,
         "mettre.";
   }

!  Yes: see No.
];

! ==============================================================================

Constant LIBRARY_FRENCH;       ! for dependency checking.
#Endif;

! ==============================================================================
